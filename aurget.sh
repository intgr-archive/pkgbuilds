#!/bin/bash
# settings
WGETOPTS=""
TARDIR="archives"

# script
die() {
  echo "abort: $@" >&2
  exit 1
}
run() {
  echo "> $@" >&2
  "$@" || die "failed command: $@"
}
prompt() {
  while true; do
    echo -n "$1 [y/N] "
    read answer
    test -z "$answer" && answer=n
    case $answer in
      [Yy])
        return 0
        ;;
      [Nn])
        return 1
        ;;
    esac
  done
}

fetch_pkg() {
  PKGNAME="$1"
  ARCHIVE="$PKGNAME.tar.gz"
  run wget $WGETOPTS "https://aur.archlinux.org/cgit/aur.git/snapshot/$ARCHIVE" -O "$TARDIR/$ARCHIVE" || return 1
  run tar xvzf "$TARDIR/$ARCHIVE" || return 1
}

fetch_pkg_sources() {
  (
  cd "$1" || return 1
  makepkg -g || return 1
  )
}

build_pkg() {
  (
  cd "$1" || return 1
  makepkg -i || return 1
  )
}

#install_pkg() {
#  sudo pacman -U
#}

# script main

test -n "$1" || die "usage: $0 PKGNAME"

for pkg in "$@"; do
  fetch_pkg $pkg || die "error fetching $pkg"
done

if prompt "Build and install packages?"; then
  for pkg in "$@"; do
    build_pkg $pkg || die "error building $pkg"
  done
else
  for pkg in "$@"; do
    fetch_pkg_sources $pkg || die "error fetching sources for $pkg"
  done
fi

